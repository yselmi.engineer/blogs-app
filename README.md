The project was done with: <br/>
    React in the frontend <br/>
    FastAPI in the backend <br/>
    MongoDB as Database (used MongoDB Atlas) <br/>

<br/>
To launch: <br/>
    in the "frontend" directory run command : npm i && npm start <br/>
    in the backend directory run command : uvicorn main:app --reload <br/>