from fastapi import FastAPI, Request, HTTPException, status
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel, Field
import uvicorn
from pymongo import MongoClient
from bson import ObjectId


app = FastAPI()


origins = [
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


class Blog(BaseModel):
    _id: str
    title: str
    content: str
    preview: str
    author: str


def convertBlog(blog: Blog):
    blog["_id"] = str(blog["_id"])
    return blog


password = "WMci7RPPOtVBJsuZ"
atlas_uri = f'mongodb+srv://yassine:{password}@cluster0.6ieprjv.mongodb.net/blogsDB'
db_name = "blogsDB"


@app.on_event("startup")
def startup_db_client():
    app.mongodb_client = MongoClient(atlas_uri)
    app.database = app.mongodb_client[db_name]


@app.on_event("shutdown")
def shutdown_db_client():
    app.mongodb_client.close()


@app.get("/blogs")
async def getBlogs(request: Request, filter: str = ""):
    blogs = request.app.database["blogs"].find()
    filter = filter.lower()
    blogs = [blog for blog in blogs if (
        filter in blog["title"].lower() or filter in blog["author"].lower() or filter in blog["content"].lower())]
    return list(map(convertBlog, blogs))


@app.get("/blogs/{id}", response_model=Blog)
async def getBlog(request: Request, id: str):
    if (blog := request.app.database["blogs"].find_one({"_id": ObjectId(id)})) is not None:
        return blog

    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                        detail=f"Blog with ID {id} not found")


@app.post("/blogs",  response_model=Blog)
async def createBlog(request: Request, blog: Blog):
    blog = blog.__dict__
    new_blog = request.app.database["blogs"].insert_one(blog)
    created_blog = request.app.database["blogs"].find_one(
        {"_id": new_blog.inserted_id}
    )
    return created_blog


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
