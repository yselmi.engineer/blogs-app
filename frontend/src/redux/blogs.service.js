import axios from "axios";

const api = axios.create({});

const API = "http://localhost:8000/blogs";

export const getblogsService = async (filter = "") => {
  const { data } = await api.get(API, { params: { filter } });
  return data;
};

export const getblogService = async (id) => {
  const { data } = await api.get(`${API}/${id}`);
  return data;
};

export const createBlogService = async (payload) => {
  const { data } = await api.post(API, payload);
  return data;
};
