import { configureStore } from "@reduxjs/toolkit";
import { applyMiddleware, combineReducers, compose } from "redux";
import blogsReducer from "./blogs.slice";
//import { loadState } from "./persist";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const preloadedState = {};

const store = configureStore({
  reducer: {
    blogs: blogsReducer,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware(),
  preloadedState,
  //enhancers: [composeEnhancers],
});

export default store;
