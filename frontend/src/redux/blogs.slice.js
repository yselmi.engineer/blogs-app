import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {
  createBlogService,
  getblogService,
  getblogsService,
} from "./blogs.service";

export const getBlogsAsync = createAsyncThunk(
  "/blogs/get/all",
  async (payload, thunkAPI) => {
    try {
      return await getblogsService(payload);
    } catch (error) {
      thunkAPI.rejectWithValue(error);
    }
  }
);

export const getBlogAsync = createAsyncThunk(
  "/blogs/get/one",
  async (payload, thunkAPI) => {
    try {
      return await getblogService(payload);
    } catch (error) {
      thunkAPI.rejectWithValue(error);
    }
  }
);

export const createBlogAsync = createAsyncThunk(
  "/blogs/create",
  async ({ onSuccess, payload }, thunkAPI) => {
    try {
      const data = await createBlogService(payload);
      onSuccess();
      return data;
    } catch (error) {
      thunkAPI.rejectWithValue(error);
    }
  }
);

const initialState = {
  items: [],
  item: undefined,
};

const slice = createSlice({
  name: "blogs",
  initialState,
  reducers: {
    clearBlog: (state, action) => {
      state.item = undefined;
    },
  },
  extraReducers: {
    [getBlogsAsync.fulfilled]: (state, action) => {
      state.items = action.payload;
    },
    [getBlogAsync.fulfilled]: (state, action) => {
      state.item = action.payload;
    },
    [createBlogAsync.fulfilled]: (state, action) => {
      state.items.push(action.payload);
    },
  },
});

export const { clearBlog } = slice.actions;
export default slice.reducer;
