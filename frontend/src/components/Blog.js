import { UpOutlined, DownOutlined } from "@ant-design/icons";
import { Avatar, Card } from "antd";
import clsx from "clsx";
import { useState } from "react";
import "./style.css";

const Blog = ({ data }) => {
  const [upvotes, setUpvotes] = useState(0);
  const [downvotes, setDownvotes] = useState(0);

  return (
    <Card
      style={{
        width: 300,
      }}
      cover={
        <img
          alt={data?.title}
          src={data?.preview}
          className={clsx("preview", {
            green: upvotes > downvotes,
            red: upvotes < downvotes,
          })}
        />
      }
      actions={[
        <div
          type="button"
          className="votes-container"
          onClick={() => setUpvotes((prev) => ++prev)}
        >
          <div className="votes">{upvotes}</div>
          <UpOutlined key="upvotes" />
        </div>,
        <div
          type="button"
          className="votes-container"
          onClick={() => setDownvotes((prev) => ++prev)}
        >
          <div className="votes">{downvotes}</div>
          <DownOutlined key="downvotes" />
        </div>,
      ]}
    >
      <Card.Meta
        avatar={
          <Avatar src="https://cdn-icons-png.flaticon.com/512/147/147142.png" />
        }
        title={data?.title}
        description={<div className="author">{data?.author}</div>}
      />
      <div className="content">
        {data?.content?.substr(0, 80)}{" "}
        <a type="button" className="view-more-btn" href={`/blogs/${data?._id}`}>
          continue reading
        </a>
      </div>
    </Card>
  );
};
export default Blog;
