import React, { useEffect, useState } from "react";
import { Button, Cascader, Input, Select, Space } from "antd";
import Blog from "./Blog";
import _ from "lodash";
import "./style.css";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getBlogsAsync } from "../redux/blogs.slice";

export default () => {
  const { items } = useSelector((state) => state?.blogs);
  const [filter, setFilter] = useState("");

  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(getBlogsAsync());
  }, []);

  const onFilterChange = (e) => {
    setFilter(e.target.value);
    dispatch(getBlogsAsync(e.target.value));
  };

  useEffect(() => {
    console.log(items);
  }, [items]);

  return (
    <div className="blogs">
      <div className="top-container">
        <Input onChange={onFilterChange} value={filter} className="filter" />
        <Button
          type="primary"
          className="create-btn"
          onClick={() => navigate("create")}
        >
          Create
        </Button>
      </div>

      <div className="grid">
        {_.map(items, (it, index) => (
          <Blog data={it} key={index} />
        ))}
      </div>
    </div>
  );
};
