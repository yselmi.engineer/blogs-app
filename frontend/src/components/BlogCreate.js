import { Button, Input } from "antd";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { createBlogAsync } from "../redux/blogs.slice";
import * as yup from "yup";
import "./style.css";
import _ from "lodash";

const validator = yup
  .object()
  .shape({
    title: yup.string().required("title is required"),
    content: yup.string().required("content is required"),
    author: yup.string().required("author is required"),
    preview: yup.string().required("preview is required"),
  })
  .required();

export default () => {
  const [form, setForm] = useState({
    title: "",
    content: "",
    author: "",
    preview: "",
  });

  const [formController, setFormController] = useState({
    disableSubmit: true,
    errors: {},
  });

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const validate = async (data) => {
    try {
      await validator.validate(data, { abortEarly: false });
      setFormController({
        disableSubmit: false,
        errors: {},
      });
    } catch (error) {
      const errors = _.chain(error?.inner)
        .map((it) => [it.path, it.message])
        .fromPairs()
        .value();
      setFormController({
        disableSubmit: !_.isEmpty(errors),
        errors,
      });
    }
  };

  const onChange = (e) => {
    setForm((prev) => {
      const update = {
        ...prev,
        [e.target.name]: e.target.value,
      };
      validate(update);
      return update;
    });
  };

  const onSubmit = async () => {
    dispatch(
      createBlogAsync({
        payload: form,
        onSuccess: () => navigate("/blogs"),
      })
    );
  };

  return (
    <div className="form-container">
      <Input
        onChange={onChange}
        value={form?.title}
        className="form-input"
        name="title"
        addonBefore="Title"
      />
      {formController.errors?.title && (
        <div className="form-error">{formController.errors.title}</div>
      )}
      <Input
        onChange={onChange}
        value={form?.author}
        className="form-input"
        name="author"
        addonBefore="Author"
      />
      {formController.errors?.author && (
        <div className="form-error">{formController.errors.author}</div>
      )}
      <Input
        onChange={onChange}
        value={form?.preview}
        className="form-input"
        name="preview"
        addonBefore="Preview URL"
      />
      {formController.errors?.preview && (
        <div className="form-error">{formController.errors.preview}</div>
      )}
      <Input.TextArea
        onChange={onChange}
        value={form?.content}
        className="form-input"
        name="content"
        autoSize={{ minRows: 4, maxRows: 6 }}
        placeholder="write content of blog here"
      />
      {formController.errors?.content && (
        <div className="form-error">{formController.errors.content}</div>
      )}

      <Button
        type="primary"
        className="form-btn"
        onClick={onSubmit}
        disabled={formController.disableSubmit}
      >
        Create blog
      </Button>
    </div>
  );
};
