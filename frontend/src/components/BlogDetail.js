import { Avatar, Card } from "antd";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { clearBlog, getBlogAsync } from "../redux/blogs.slice";

export default () => {
  const { item: data } = useSelector((state) => state?.blogs);

  const dispatch = useDispatch();

  const { id } = useParams();

  useEffect(() => {
    return () => dispatch(clearBlog());
  }, []);

  useEffect(() => {
    console.log(id);
    if (id) {
      dispatch(getBlogAsync(id));
    }
  }, [id]);

  if (!data) {
    return <></>;
  }

  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Card
        style={{
          marginTop: 20,
          width: 700,
          borderWidth: 0,
        }}
        cover={<img alt={data?.title} src={data?.preview} />}
      >
        <Card.Meta
          style={{ marginBottom: 40 }}
          avatar={
            <Avatar src="https://cdn-icons-png.flaticon.com/512/147/147142.png" />
          }
          title={data?.title}
          description={<div className="author">{data?.author}</div>}
        />
        <div className="content">{data?.content}</div>
      </Card>
    </div>
  );
};
