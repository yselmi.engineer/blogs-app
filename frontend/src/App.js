import React from "react";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";
import _ from "lodash";
import { Provider } from "react-redux";
import store from "./redux/store";
import Blogs from "./components/Blogs";
import BlogDetail from "./components/BlogDetail";
import BlogCreate from "./components/BlogCreate";

const routes = [
  {
    path: "/",
    exact: true,
    element: <Navigate to="/blogs" replace />,
  },
  {
    path: "/blogs/create",
    exact: true,
    element: <BlogCreate />,
  },
  {
    path: "/blogs/:id",
    exact: true,
    element: <BlogDetail />,
  },
  {
    path: "/blogs",
    exact: true,
    element: <Blogs />,
  },
  {
    path: "*",
    exact: true,
    element: <Navigate to="/blogs" replace />,
  },
];

function App() {

  return (
    <Provider store={store}>
      <Router baseName="/">
        <Routes>
          {_.map(routes, (route) => (
            <Route {...route} />
          ))}
        </Routes>
      </Router>
    </Provider>
  );
}

export default App;
